Elma Moodle
===========

A customised build of the Moodle Stable 26 branch (2.6.4+).

This build requires the following plugins installed as git submodules:

1) Ease Auth
   /auth/ease
   https://bitbucket.org/uoesps/sps_moodle_auth_ease

2) EasyDB Auth
   /auth/easydb
   https://bitbucket.org/uoesps/sps_moodle_auth_easydb

3) SSPS Theme
   /theme/ssps
   https://bitbucket.org/uoesps/sps_moodle_theme_ssps

4) Ajax Marking
   /blocks/ajax_marking
   https://github.com/mattgibson/moodle-block_ajax_marking

5) Configurable Reports
   /blocks/configurable_reports
   https://github.com/jleyva/moodle-block_configurablereports

6) Turnitintooltwo
   mod/turnitintooltwo
   https://bitbucket.org/uoesps/moodle_mod_turnitintooltwo
   
7) Turnitin Plagiarism
   /plagiarism/turnitin
   https://bitbucket.org/uoesps/moodle_plagiarism_turnitin
 
8) SPS Mod/Assign
   mod/assign
   https://bitbucket.org/uoesps/sps_moodle_mod_assign

9) SPS User Enrolments
   local/userenrols
   https://bitbucket.org/uoesps/sps_moodle_local_userenrols

DEPLOYMENT
==========

1) Clone repo to the server:
   > git clone https://bitbucket.org/uoesps/elma_moodle_26 <checkout_dir>
   
2) Switch to the required branch
   > cd <checkout_dir>
   > git fetch
   > git checkout <req_branch>

3) Clone submodules:
   > git submodule init
   > git submodule update
   
If this is a fresh install, then:

4) Follow the Moodle installation process

Else, if its a restoration:

4) Restore a copy of the database as defined in config.php

5) Restore a copy of the data directory as defined in config.php